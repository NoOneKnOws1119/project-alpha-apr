from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm


@login_required
def show_project(request, id):
    tasks = Task.objects.get(id=id)
    context = {"tasks": tasks}
    return render(request, "projects/detail.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.assignee = request.user
            task.save()
            return redirect("home")
    else:
        form = TaskForm()
    context = {"form": form}

    return render(request, "tasks/create-view.html", context)


@login_required
def task_list(request):
    list_task = Task.objects.filter(assignee=request.user)
    context = {"list_tasks": list_task, "number": len(list_task)}
    return render(request, "tasks/mine.html", context)
